﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace TodoApi.Models
{
    //[Table("todolist_db")]
    public class TodoItem
    {
        public int id { get; set; }

        public string kegiatan { get; set; }

        public DateTime tanggal { get; set; }

        public string jam { get; set; }

        public string keterangan { get; set; }

        public string status { get; set; }
    }
}
